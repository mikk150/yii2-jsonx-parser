Yii2-JSONx parser
==========

Have you ever thought - How to mess with your fellow developers? How to enjoy big-blue's amazing idea "how to merge inferior XML with vastly superior JSON?". Fear not, JSONx is for you!

Yii2 module for parsing JSONx requests.

Installation:
-------------
The preferred way to install this extension is through [composer](https://getcomposer.org/download/).

Either run
```
php composer.phar require --prefer-dist mikk150/yii2-jsonx-parser "^1.0.0"
```

or add

```
"mikk150/yii2-jsonx-parser": "^1.0.0"
```
to the require section of your composer.json.

Configuration:
--------------
```php
    'components' => [
        'request' => [
            'parsers' => [
                'application/xml' => mikk150\jsonx\JsonXParser::class,
            ]
        ]
    ]