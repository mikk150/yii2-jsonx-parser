<?php

namespace tests\unit;

use Codeception\Test\Unit;
use Exception;
use mikk150\jsonx\JsonXParser;
use yii\web\BadRequestHttpException;

class JsonXParserTest extends Unit
{

    /**
     * @dataProvider jsonXprovider
     */
    public function testParsingData($jsonx, $array)
    {
        $jsonXParser = new JsonXParser();

        $this->assertEquals($array, $jsonXParser->parse($jsonx, 'application/xml'));
    }

    public function jsonXprovider()
    {
        return [
            'array' => [
                '<?xml version="1.0" encoding="UTF-8"?>
                <json:object xsi:schemaLocation="http://www.datapower.com/schemas/json jsonx.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:json="http://www.ibm.com/xmlns/prod/2009/jsonx">
                    <json:array name="phoneNumbers">
                        <json:string>212 555-1111</json:string>
                        <json:string>212 555-2222</json:string>
                    </json:array>
                </json:object>',
                [
                    'phoneNumbers' => [
                        '212 555-1111',
                        '212 555-2222'
                    ]
                ]
            ],
            'empty-object' => [
                '<?xml version="1.0" encoding="UTF-8"?>
                <json:object xsi:schemaLocation="http://www.datapower.com/schemas/json jsonx.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:json="http://www.ibm.com/xmlns/prod/2009/jsonx" />',
                []
            ],
            'boolean' => [
                '<?xml version="1.0" encoding="UTF-8"?>
                <json:object xsi:schemaLocation="http://www.datapower.com/schemas/json jsonx.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:json="http://www.ibm.com/xmlns/prod/2009/jsonx">
                    <json:boolean name="remote">false</json:boolean>
                </json:object>',
                [
                    'remote' => false
                ],
            ],
            'null' => [
                '<?xml version="1.0" encoding="UTF-8"?>
                <json:object xsi:schemaLocation="http://www.datapower.com/schemas/json jsonx.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:json="http://www.ibm.com/xmlns/prod/2009/jsonx">
                    <json:null name="additionalInfo" />
                </json:object>',
                [
                    'additionalInfo' => null
                ],
            ],
            'number' => [
                '<?xml version="1.0" encoding="UTF-8"?>
                <json:object xsi:schemaLocation="http://www.datapower.com/schemas/json jsonx.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:json="http://www.ibm.com/xmlns/prod/2009/jsonx">
                    <json:number name="height">62.4</json:number>
                    <json:number name="width">-10</json:number>
                </json:object>',
                [
                    'height' => 62.4,
                    'width' => -10,
                ]
            ],
            'string' => [
                '<?xml version="1.0" encoding="UTF-8"?>
                <json:object xsi:schemaLocation="http://www.datapower.com/schemas/json jsonx.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:json="http://www.ibm.com/xmlns/prod/2009/jsonx">
                    <json:string name="Ticker">IBM</json:string>
                </json:object>',
                [
                    'Ticker' => 'IBM'
                ]
            ],
            'just-null' => [
                '<?xml version="1.0" encoding="UTF-8"?>
                <json:null xsi:schemaLocation="http://www.datapower.com/schemas/json jsonx.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:json="http://www.ibm.com/xmlns/prod/2009/jsonx" />',
                null
            ],
            'just-string' => [
                '<?xml version="1.0" encoding="UTF-8"?>
                <json:string xsi:schemaLocation="http://www.datapower.com/schemas/json jsonx.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:json="http://www.ibm.com/xmlns/prod/2009/jsonx">IBM</json:string>',
                'IBM'
            ],
            'just-boolean' => [
                '<?xml version="1.0" encoding="UTF-8"?>
                <json:boolean xsi:schemaLocation="http://www.datapower.com/schemas/json jsonx.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:json="http://www.ibm.com/xmlns/prod/2009/jsonx">true</json:boolean>',
                true
            ],
            'just-number' => [
                '<?xml version="1.0" encoding="UTF-8"?>
                <json:number xsi:schemaLocation="http://www.datapower.com/schemas/json jsonx.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:json="http://www.ibm.com/xmlns/prod/2009/jsonx">-3.22</json:number>',
                -3.22
            ],
            'just-xml' => [
                '<?xml version="1.0" encoding="UTF-8"?><note><to>Tove</to></note>',
                null
            ]
        ];
    }

    /**
     * @dataProvider invalidJsonXProvider
     */
    public function testParsingInvalidData($invalidJsonX)
    {
        $this->expectException(BadRequestHttpException::class);

        $jsonXParser = new JsonXParser();

        $jsonXParser->parse($invalidJsonX, 'application/xml');
    }

    public function invalidJsonXProvider()
    {
        return [
            'just-random-string' => ['random string'],
            'just-xml-header' => ['<?xml version="1.0" encoding="UTF-8"?>'],
        ];
    }
}
