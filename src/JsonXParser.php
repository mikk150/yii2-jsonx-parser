<?php

namespace mikk150\jsonx;

use DOMDocument;
use function JSONx\parse;
use yii\base\BaseObject;
use yii\web\BadRequestHttpException;
use yii\web\RequestParserInterface;

class JsonXParser extends BaseObject implements RequestParserInterface
{
    public function parse($rawBody, $contentType) {
        $dom = new DOMDocument();

        try {
            $dom->loadXML($rawBody);
        } catch (\Throwable $th) {
            throw new BadRequestHttpException('Invalid JSONx data in request body: ' . $th->getMessage());
        }

        return parse($dom->documentElement);
    }
}
